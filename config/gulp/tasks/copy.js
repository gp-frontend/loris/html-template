let paths = {
  'src': 'src/web/static/**/*',
  'dest': 'build/',
};

module.exports = () => {

  $.gulp.task('copy', () => {

    return $.gulp.src(paths.src)
      .pipe($.gulp.dest(paths.dest))
  });
};
