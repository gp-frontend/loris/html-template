// This is gulp tasks for postcss only
let
  cssImport = require("postcss-import"),
  cssvariables = require('postcss-css-variables'),
  nested = require('postcss-nested'),
  postcssColorMod = require('postcss-color-mod-function'),
  postcssPresetEnv = require('postcss-preset-env'),
  mqpacker = require('css-mqpacker'),
  sortCSSmq = require('sort-css-media-queries'),
  rucksack = require('rucksack-css'),
  automath = require('postcss-automath'),
  bootstrap = require("postcss-bootstrap-4-grid"),
  postcssNormalize = require("postcss-normalize"),
  reporter = require("postcss-reporter"),
  stylesPath = {
      'src': 'src/website/*.pcss', // <= change
      'srcCSS': 'build/css/*.css',
      'dest': 'build/css',

      'srcBootstrapGridTemplate': 'src/website/utils/grid/template/bootstrap-template.pcss', // <= change
      'destBootstrapGrid': 'src/website/utils/grid/', // <= change

      'srcNormalizeTemplate': 'src/website/utils/normalize/template/normalize-template.pcss', // <= change
      'destNormalize': 'src/website/utils/normalize/', // <= change
  };

const postcssPlugins = [
  cssImport,
  rucksack,
  automath,
  postcssColorMod,
  nested,
  cssvariables,
  postcssPresetEnv({
    stage: 0,
    browsers: 'last 1 versions',
    features: {
      'custom-properties': {
        preserve: false
      }
    }
  }),
  mqpacker({ // group media query rules
    sort: sortCSSmq.desktopFirst
  }),
  reporter({
    clearReportedMessages: true
  })
];

module.exports = () => {

  // DEV
  $.gulp.task('styles:dev', function () {

    return $.gulp.src(stylesPath.src)

      .pipe($.plugins.plumber({

        errorHandler: function(err) {

          $.plugins.notify.onError({
            title: "Error in styles",
            message: "<%= error.message %>"
          })(err);
        }
      }))

      .pipe($.plugins.sourcemaps.init())

      .pipe($.plugins.postcss(postcssPlugins))

      .pipe($.plugins.rename({ extname: '.css'}))

      .pipe($.gulp.dest(stylesPath.dest))

      .pipe($.plugins.csso({
        discardComments: {
          removeAll: true
        }
      }))

      .pipe($.plugins.rename({
          extname: '.min.css'
        }),)

      .pipe($.plugins.sourcemaps.write('.'))

      .pipe($.gulp.dest(stylesPath.dest))

      .pipe($.browserSync.stream());
  });

  // PROD
  $.gulp.task('styles:prod', function () {

    return $.gulp.src(stylesPath.src)

      .pipe($.plugins.plumber({

        errorHandler: function(err) {

          $.plugins.notify.onError({
            title: "Error in styles",
            message: "<%= error.message %>"
          })(err);
        }
      }))

      .pipe($.plugins.postcss(postcssPlugins))

      .pipe($.plugins.stylelint({
        fix: true,
        reporters: [{
            formatter: 'string',
            console: true
          }]
      }))

      .pipe($.plugins.rename({ extname: '.css'}))

      .pipe($.gulp.dest(stylesPath.dest))

      .pipe($.plugins.csso({
        discardComments: {
          removeAll: true
        }
      }))

      .pipe($.plugins.rename({
          extname: '.min.css'
        }),)

      .pipe($.gulp.dest(stylesPath.dest));
  });

  // Generate Bootstrap Grid
  $.gulp.task('styles:bootstrap', function () {

    return $.gulp.src(stylesPath.srcBootstrapGridTemplate)

      .pipe($.plugins.postcss([

        bootstrap({
          gridColumns: 12,
          gridGutterWidth: '30px',
          containerMaxWidths: {
            sm: '1170px'
          },
          gridBreakpoints: {
            xs: '600px',
            sm: '768px',
            md: '980px',
            lg: '1024px',
            xl: '1200px'
          }
        })

      ]))

      .pipe($.plugins.rename({
          basename: "bootstrap-grid",
          extname: '.pcss'
        }),
      )

      .pipe($.gulp.dest(stylesPath.destBootstrapGrid));
  });

  // Generate Normalize.css
  $.gulp.task('styles:normalize', function () {

    return $.gulp.src(stylesPath.srcNormalizeTemplate)

      .pipe($.plugins.postcss([

        postcssNormalize({
          browsers: 'last 2 versions'
        })

      ]))

      .pipe($.plugins.rename({
          basename: "normalize",
          extname: '.pcss'
        }),
      )

      .pipe($.gulp.dest(stylesPath.destNormalize));
  });

  // Generate styles Boostrap & Normalize
  $.gulp.task('styles:generate', $.gulp.series(

    $.gulp.parallel(
      'styles:normalize',
      'styles:bootstrap',
    )
  ));
};
