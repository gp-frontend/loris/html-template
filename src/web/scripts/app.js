(function ($) {

  /*----------------------------------------
   TRANSITION SCROLL
 ----------------------------------------*/
  $('.js-link-scroll').on('click', function () {

    var anchor = $(this),
        headerHeight = $(".header").height();

    $('html, body').stop().animate({
      scrollTop: $(anchor.attr('href')).offset().top - headerHeight - 40
    }, 1000)
  });

  /*----------------------------------------
    SCROLL BAR WIDTH
  ----------------------------------------*/

  //WIDTH SCROLL BAR
  var scrollBar = document.createElement('div');

  scrollBar.style.overflowY = 'scroll';
  scrollBar.style.width = '50px';
  scrollBar.style.height = '50px';

  // при display:none размеры нельзя узнать
  // нужно, чтобы элемент был видим,
  // visibility:hidden - можно, т.к. сохраняет геометрию
  scrollBar.style.visibility = 'hidden';

  document.body.appendChild(scrollBar);
  var scrollWidth = scrollBar.offsetWidth - scrollBar.clientWidth;
  document.body.removeChild(scrollBar);

  /*----------------------------------------
    POPUPS / MODALS
  ----------------------------------------*/
  (function () {

    var ESC_KEYCODE = 27;

    var popup = $(".popup"),
      popupOpen = $("[data-popup='open']"),
      popupClose = $("[data-popup='close']"),
      popupOverlay = $(".popup__overlay");

    /* POPUP FUNCTIONS */
    $.fn.gpPopup = function( method ) {

      if ( methods[method] ) {
        return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
      } else if ( typeof method === 'object' || ! method ) {
        return methods.show.apply( this, arguments );
      } else {
        $.error( 'Метод с именем ' +  method + ' не существует' );
      }
    };

    // alert( scrollWidth );

    /* POPUP METHODS */
    var methods = {
      //show popup
      show : function( options ) {
        return this.each(function(){

          popup.removeClass('popup--open');
          $(this).addClass('popup--open');
          $('body').addClass('page--locked');

          if(scrollWidth > 0){
            $('body').css('padding-right', scrollWidth);
            $('header').css('padding-right', scrollWidth);
          }
        });
      },
      //hide all popups
      hide : function( ) {

        return this.each(function(){

          if (popup.hasClass('popup--open')) {
            popup.removeClass('popup--open');
            $('body').removeClass('page--locked').css('padding-right', '');
            $('header').css('padding-right', '');
          }
        })
      }
    };

    /* OPEN */
    function openPopup(e){
      e.preventDefault();
      var id = $(this).attr('data-popup-id');
      $(id).gpPopup();
    }

    /* CLOSE */
    function closePopup(e){
      e.preventDefault();
      popup.gpPopup('hide');
    }

    /* ADD CLICK FUNCTION */
    popupOpen.click(openPopup);
    popupClose.click(closePopup);
    popupOverlay.click(closePopup);

    /* PRESS ESC BUTTON */
    $(document).keydown(function(evt) {
      if( evt.keyCode === ESC_KEYCODE ) {
        popup.gpPopup('hide');
        return false;
      }
    });
  })();

  /*----------------------------------------
    INPUT MASK
  ----------------------------------------*/
  $(".js-mask-number").mask("0#");
  $(".js-mask-phone").mask("+7 (000) 000-00-00");
  $(".js-mask-time").mask("00 : 00");
  $(".js-mask-date").mask("00.00.0000");

  /*----------------------------------------
    MENU
  ----------------------------------------*/
  var menu = $("#menu"),
      buttonMenu = $(".js-button-menu"),
      linkMenu = $(".menu__link"),
      buttonSubmenu = $(".menu__button-dropdown");

  function openMenu() {
    menu.toggleClass("menu--open");
    buttonMenu.toggleClass("button-menu--active");
    closeActionTooltip();
    $("body").toggleClass("page--locked");

    if(scrollWidth > 0){
      $('body').css('padding-right', scrollWidth);
      $('header').css('padding-right', scrollWidth);
    }
  }

  function closeMenu() {
    menu.removeClass("menu--open");
    buttonMenu.removeClass("button-menu--active");
    $("body").removeClass("page--locked");

    if(scrollWidth > 0){
      $('body').css('padding-right', scrollWidth);
      $('header').css('padding-right', scrollWidth);
    }
  }

  function openSubmenu() {

    var id = $(this).attr('data-submenu');

    $(this)
      .addClass("menu__link--active")
      .parent()
      .siblings()
      .find(".menu__link")
      .removeClass("menu__link--active");

    $(id)
      .addClass("submenu--open")
      .siblings()
      .removeClass("submenu--open");
  }

  buttonMenu.on("click", openMenu);

  linkMenu.on("click", closeMenu);

  $(window).on('load resize', function() {

    if ($(window).width() > 600 - scrollWidth) {

      linkMenu.on("mouseenter", openSubmenu);

    } else  {

      buttonSubmenu.on("click", function () {

        menu.addClass("menu--open");
        $(".menu__right").addClass("menu__right--open-on-mobile");

        var id = $(this).attr('data-submenu');

        $(this)
          .siblings(".menu__link")
          .addClass("menu__link--active")
          .parent()
          .siblings()
          .find(".menu__link")
          .removeClass("menu__link--active");

        $(id)
          .addClass("submenu--open")
          .siblings()
          .removeClass("submenu--open");

      })

      linkMenu.on("click", );
    }
  });

  $(".menu__back-button").on("click", function () {
    $(".menu__right").removeClass("menu__right--open-on-mobile");
  });

  /*----------------------------------------
    PANEL ACTION
  ----------------------------------------*/
  var buttonActionToggle = $(".panel-action__button-toggle");

  function closeActionTooltip() {

    buttonActionToggle
      .removeClass("panel-action__button-toggle--selected");

    $(".tooltip-info")
      .removeClass("tooltip-info--show");
  }

  function openActionTooltip() {

    closeMenu();

    if(
      $(this).hasClass("panel-action__button-toggle--selected")
    ) {

      closeActionTooltip();

    } else {

      closeActionTooltip();

      $(this)
        .addClass("panel-action__button-toggle--selected")
        .closest(".panel-action__item")
        .find(".tooltip-info")
        .addClass("tooltip-info--show");
    }
  }

  buttonActionToggle.on("click", openActionTooltip);

  /*----------------------------------------
    SWITCH LOGIN / FORGET
  ----------------------------------------*/
  var
    linkForget = $(".js-link-forget"),
    linkLogin = $(".js-link-login");

  linkForget.on("click", function () {
    $("#form-login").removeClass("form--show");
    $("#form-recovery").addClass("form--show");
  });

  linkLogin.on("click", function () {
    $("#form-recovery").removeClass("form--show");
    $("#form-login").addClass("form--show");
  });


  /*----------------------------------------
   CAROUSELS
  ----------------------------------------*/

  // slider
  var $instruments = $(".js-carousel-instruments");

  settings_slider = {
    rows: 0,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: false,
    mobileFirst: true,
    infinite: false,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2
        }
      },
    ]
  };

  // slick on mobile
  function slick_on_mobile(slider, settings){

    $(window).on('load resize', function() {

      if ($(window).width() > 1024 - scrollWidth) {

        if (slider.hasClass('slick-initialized')) {
          slider.slick('unslick');
        }

        return
      }
      if (!slider.hasClass('slick-initialized')) {
        return slider.slick(settings);
      }
    });
  }

  slick_on_mobile($instruments, settings_slider);

  /*----------------------------------------
   SPECIAL CAROUSEL
  ----------------------------------------*/

  $(".js-carousel-cards").slick({
    rows: 0,
    dots: true,
    arrows: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: false,
    responsive: [
      {
        breakpoint: 1300,
        settings: {
          arrows: false
        }
      },
      {
        breakpoint: 1280,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          arrows: false
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 980,
        settings: {
          slidesToShow: 2,
          arrows: false
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          arrows: false
        }
      }
    ]
  });

  /*----------------------------------------
   NEWS CAROUSEL
  ----------------------------------------*/

  $(".js-carousel-news").slick({
    rows: 0,
    dots: true,
    arrows: true,
    initialSlide: 1,
    centerMode: true,
    variableWidth: true,
    infinite: false,
    prevArrow: '<button type="button" class="slick-prev slick-prev--small">Previous</button>',
    nextArrow: '<button type="button" class="slick-next slick-next--small">Next</button>',
    appendArrows: ".slick-controls__arrows",
    appendDots: ".slick-controls",
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          arrows: false
        }
      },
    ]
  });

  /*----------------------------------------
   GALLERY CAROUSEL
  ----------------------------------------*/

  $('.js-gallery-preview').slick({
    rows: 0,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    asNavFor: '.js-gallery-thumbnails'
  });

  $('.js-gallery-thumbnails').slick({
    rows: 0,
    slidesToShow: 4,
    slidesToScroll: 1,
    centerMode: false,
    asNavFor: '.js-gallery-preview',
    arrows: false,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
        }
      },
    ]
  });

  /*----------------------------------------
   POPUP COUNTRIES
  ----------------------------------------*/
  var buttonPopupCountriesOpen = $(".js-button-countries-open"),
      buttonPopupCountriesClose = $(".js-button-countries-close"),
      popupCountries = $("#popup-countries");

  function openPopupCountries() {
    popupCountries.addClass("popup-countries--open");
  }

  function closePopupCountries() {
    popupCountries.removeClass("popup-countries--open");
  }

  buttonPopupCountriesOpen.on("click", openPopupCountries);
  buttonPopupCountriesClose.on("click", closePopupCountries);

  /*----------------------------------------
   VALIDATE FORMS
  ----------------------------------------*/

  $('#form-call').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function () {
      $('#popup-thanks').gpPopup();
    }
  });

  $('#form-login').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function () {
      $('#popup-thanks').gpPopup();
    }
  });

  $('#form-recovery').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function () {
      $('#popup-thanks').gpPopup();
    }
  });

  $('#form-subscribe').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function () {
      $('#popup-thanks').gpPopup();
    }
  });

  $('#form-feedback').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function () {
      $('#popup-thanks').gpPopup();
    }
  });

  $('#form-consultant').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function () {
      $('#popup-thanks').gpPopup();
    }
  });

  // Click outside ---------------------------------

  var clickEvent = ((document.ontouchstart!==null)?'click':'touchstart');

  $(document).on(clickEvent, function(e) {

    // if (!$(e.target ).closest(".popup-countries--open").length) {
    //   closePopupCountries();
    // }

    if (!$(e.target ).closest(".header").length) {
      closeActionTooltip();
    }

    e.stopPropagation();
  });
  // --------------------------------- end click outside
})(jQuery);
